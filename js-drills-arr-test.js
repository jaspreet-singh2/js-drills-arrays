var cFunc = require("./js-drills-Arrays").modules;
const items = [1, 2, 3, 4, 5, 5];

console.log("\x1b[32m%s\x1b[0m", "function each(elements, cb)");
cFunc.each(items, (x) => {
  console.log(x);
});

console.log("\x1b[32m%s\x1b[0m", "function map(elements, cb) ");
var mapList = cFunc.map(items, (x) => {
  return x * x;
});
console.log(mapList);

console.log("\x1b[32m%s\x1b[0m", "function reduce(elements, cb, startingValue)");
var reduced = cFunc.reduce(items, (memo, x) => {
  return x * memo;
});
console.log(reduced);

console.log("\x1b[32m%s\x1b[0m", "function find(elements, cb)");

var find = cFunc.find(items, (x) => x % 2 == 0);
console.log(find);

console.log("\x1b[32m%s\x1b[0m", "function filter(elements, cb)");

var filter = cFunc.filter(items, (x) => x % 2 == 0);
console.log(filter);

console.log("\x1b[32m%s\x1b[0m", "function flatten(elements)");

const nestedArray = [1, [2], [[3]], [[[4]]]];
var flatten = cFunc.flatten(nestedArray);
console.log(flatten);
